const merge             = require('webpack-merge')
const defaultConfig     = require('./webpack.common.js')
const webpack           = require('webpack')
const WebpackHtmlPlugin = require('html-webpack-plugin')
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
const path              = require('path')
const Dotenv            = require('dotenv-webpack')
const { buildHotLoaderConfig } = require('./utils')

console.log(buildHotLoaderConfig())
module.exports = merge(defaultConfig, {
    entry : {
        app : [
            buildHotLoaderConfig(),
            './src/main.ts'
        ]
    }, 
    mode  : (process.env.MODE).toLowerCase(),
    plugins : [
        // Set index template
        new WebpackHtmlPlugin({
            filename : 'index.html',
            template : 'src/index.html',
            inject   : true,
        }),

        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        
        new webpack.NoEmitOnErrorsPlugin(),

        new FriendlyErrorsWebpackPlugin(),

        new Dotenv({
            path: path.resolve(__dirname, '..', '.env.'+(process.env.MODE).toLowerCase())
        })
    ]
})