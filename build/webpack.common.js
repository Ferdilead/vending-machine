const path      = require('path')
const fs        = require('fs')
const { CheckerPlugin, TsConfigPathsPlugin } = require('awesome-typescript-loader')

module.exports = {
    output  : {
        // path : path.join(__dirname, '..', 'public/'),
        path : path.join(process.cwd(), '..', 'public/'),
        publicPath : '/public/',
        filename : 'dist/[name]?v=[hash:4].js'
    },
    devtool: "inline-cheap-module-source-map",
    // resolve file
    resolve : {
        extensions : ['.ts', '.vue', '.json', '.js'],
        alias : {
            'vue$': 'vue/dist/vue.esm.js' 
        },
        plugins : [
            new TsConfigPathsPlugin()
        ]
    },
    module  : {
        rules : [
            {
                test    : /\.ts$/,
                exclude : /node_modules/,
                include : path.resolve(__dirname, '..', 'src'),
                use     : [
                    {
                        loader  : 'babel-loader',
                        options : JSON.parse(fs.readFileSync(path.join(__dirname, '../', '.babelrc')))
                    },
                    {
                        loader  : 'awesome-typescript-loader',
                        options : {
                            appendTsSuffixTo: [/\.vue$/],
                        }
                    }
                ]
            },
            {
                test    : /\.html$/,
                exclude : path.resolve('src/index.html'),
                use     : [
                    {
                        loader  : 'vue-template-loader',
                        options : {
                            scoped : true
                        },
                    }
                ]
                
            },
            {
                // Loaders that transform css into a format for webpack consumption should be post loaders (enforce: 'post')
                enforce: 'post',
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
                // We needed to split the rule for .scss files across two rules
            {
                // The loaders that compile to css (postcss and sass in this case) should be left as normal loaders
                test: /\.scss$/,
                use: [ 
                    // { 
                    //     loader  : 'postcss-loader', 
                    //     options : { 
                    //         plugins : {
                    //             // to edit target browsers: use "browserslist" field in package.json
                    //             "autoprefixer": {}
                    //         }
                    //     } 
                    // }, 
                    'sass-loader']
            },
            {
                // The loaders that format css for webpack consumption should be post loaders
                enforce: 'post',
                test: /\.scss$/,
                use: ['style-loader', 'css-loader']
            }
        ]
    }
    // plugins: [
        
    //     // This for resolve aliases by awesome-typescript-loader
    //     new CheckerPlugin()
    // ]
}