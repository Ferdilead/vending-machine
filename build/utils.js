const hotLoaderConfig = require('./../config/hot-loader')

exports.buildHotLoaderConfig = () => {
    let param = []

    for(let i in hotLoaderConfig) {
        param.push(encodeURIComponent(i) + '=' + encodeURIComponent(hotLoaderConfig[i]))
    }

    return 'webpack-hot-middleware/client?' + param.join('&')
}