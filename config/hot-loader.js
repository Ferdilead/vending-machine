const overlayStyles = {
    background: '#000000'
}

module.exports = {
    timeout         : 100,
    log             : false,
    reload          : true,
    overlayStyles   : JSON.stringify(overlayStyles)
}