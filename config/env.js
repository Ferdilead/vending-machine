const path   = require('path')
const env    = require('dotenv').config({path: path.resolve(__dirname, '..', '.env.'+(process.env.MODE).toLowerCase())})

module.exports = { env : env.parsed }