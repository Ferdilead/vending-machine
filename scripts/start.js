'use strict'

const webpack               = require('webpack')
const path                  = require('path')
const config                = require('./../build/webpack.dev.js')
const statsConfig           = require('./../config/stats')
const express               = require('express')
const webpackDevMiddleware  = require('webpack-dev-middleware')
const webpackHotMiddleware  = require('webpack-hot-middleware')
const chalk                 = require('chalk')
const portfinder            = require('portfinder')
const fixedWidthString      = require('fixed-width-string')

const port = process.env.port ? process.env.port : 3000

// Init server
const app   = express()

// Setup webpack
const compiler = webpack(config)

const devMiddleware = webpackDevMiddleware(compiler,{
    publicPath  : config.output.publicPath,
    stats       : statsConfig,
    minimal     : true,
    quiet       : true
})

const hotMiddleware = webpackHotMiddleware(compiler)

// Assign dev middleeware
app.use(devMiddleware)


// Assign hot middleware
app.use(hotMiddleware)

console.log(chalk.blue.inverse('  WAIT  '), chalk.blue.bold('Compiling ...'));

let server,
    url,
    _resolve,
    _reject
    
let _readyPromise = new Promise((resolve, reject) => {
        _resolve = resolve
        _reject  = reject
    })

portfinder.basePort = port

devMiddleware.waitUntilValid((val) => {
    portfinder.getPort((err, port) => {
        
        // Is port in use?
        if (err) _reject(err)

        // Catch root router with express
        app.use('/', function (req, res, next) {
            var filename = path.join(compiler.outputPath,'index.html');
            compiler.outputFileSystem.readFile(filename, (err, result) => {
              if (err) {
                return next(err);
              }
              res.set('content-type','text/html')
              res.send(result)
              res.end()
            });
        });
    
        url     = 'http://localhost:' + port

        // Start express
        server  = app.listen(port, () => {
            console.log('Development server started. Visit ' + chalk.bold.green.underline(url))
        })
        _resolve()
    })
})

module.exports = {
  ready: _readyPromise,
  close: () => {
    server.close()
  }
}