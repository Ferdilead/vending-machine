import { Vue, Component } from 'vue-property-decorator'
import { Action, Getter } from 'vuex-class'
import WithRender from './template.html?style=./style.scss'
import { format } from '@/utils/number'

@WithRender
@Component
export default class AccountBalance extends Vue {
  public tempBalance: any = 0

  @Getter('BalanceModule/getBalance')
  public balance!: number
  
  @Getter('BalanceModule/getAllowedMoney')
  public allowedMoney!: any

  @Action('BalanceModule/setIncrementBalance')
  public setBalance: any

  public handleInputChange(el: any): void {
    this.tempBalance = el.target.value as number
  } 

  public handleBalance(): void {
    const elInputBalance: HTMLInputElement = this.$refs.inputBalance as HTMLInputElement

    if(!this.validateMoney()) 
      alert('Your bills are not accepted :(')
    else 
      this.setBalance(this.tempBalance)
    
    elInputBalance.value = ''
  } 

  public validateMoney(money?: number): any {
    return this.allowedMoney.includes(parseInt(this.tempBalance))
  }

  public formatCurrency(value: number): string {
    return format(value, 0, 3, '.')
  }
}