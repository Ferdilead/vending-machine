import { Vue, Component } from 'vue-property-decorator'
import { Getter, Action } from 'vuex-class'
import WithRender from './template.html?style=./style.scss'
import { IProduct } from '@/store/modules/productModule'
import { ICart } from '@/store/modules/cartModule'
import { format } from '@/utils/number'

@WithRender
@Component
export default class Checkout extends Vue { 
  
  @Getter('BalanceModule/getBalance')
  public balance!: number

  @Getter('ProductModule/getProduct')
  public products!: IProduct[]

  @Getter('CartModule/getCarts')
  public carts!: ICart

  @Action('BalanceModule/setDecrementBalance')
  public decrementBalance!: any
  
  @Action('CartModule/addProductToCart')
  public clearCart!: any

  @Action('ProductModule/setProductQty')
  public setProductQty!: any

  public formatCurrency(value: number): string {
    return format(value, 0, 3, '.')
  }

  public handlePay(): any {
    if((this.cutOffBalance < 0)) {
      alert('Your money is not enough :(')
     
      return false 
    }

    this.processCheckout()
  }

  public processCheckout(): void { 
    const productId: number = this.filteredProduct[0].productId
    const productPrice: number = this.filteredProduct[0].price

    this.decrementBalance(productPrice)
    this.clearCart(0)
    this.setProductQty(productId)
  }

  public get filteredProduct(): IProduct[] {
    const productId: number = this.carts.productId
    
    return this.products.filter(product => product.productId === productId)
  }

  public get cutOffBalance(): number {
    return this.balance - this.filteredProduct[0].price
  }
}