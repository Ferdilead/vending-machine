import { Vue, Component, Prop } from 'vue-property-decorator'
import { Getter, Action } from 'vuex-class'
import WithRender from './template.html?style=./style.scss'
import { IProduct } from '@/store/modules/productModule'
import { ICart } from '@/store/modules/cartModule'
import { format } from '@/utils/number'

@WithRender
@Component
export default class Product extends Vue {

  @Prop([Object])
  public product!: IProduct

  @Getter('CartModule/getCarts')
  public carts!: ICart

  @Action('CartModule/addProductToCart')
  public addProduct: any

  public handleClick(productId: number): void {
    if(this.product.qty) 
      this.addProduct(productId)
    else
      alert('Out of stock :(')
  }

  public formatCurrency(value: number): string {
    return format(value, 0, 3, '.')
  }
}