import { Vue, Component } from 'vue-property-decorator'
import { Getter } from 'vuex-class'
import WithRender from './template.html?style=./style.scss'
import ProductItem from './ProductItem'
import { IProduct } from '@/store/modules/productModule'

@WithRender
@Component({
  components: {
    ProductItem
  }
})
export default class Product extends Vue {
  
  @Getter('ProductModule/getProduct')
  public products!: IProduct[]
}