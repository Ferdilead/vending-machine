import { Vue, Component } from 'vue-property-decorator'
import WithRender from './app.template.html?style=./app.scss'
import Machine from '@/layouts/Machine'

@WithRender
@Component({
  components: { Machine }
})
export default class App extends Vue {

} 