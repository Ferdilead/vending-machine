import Vue from 'vue'
import App from '@/app'
import Store from '@/store'

new Vue({
  store: Store,
  render: (h) => h(App)
}).$mount('#app')