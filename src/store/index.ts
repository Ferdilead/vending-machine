import Vue from 'vue'
import Vuex from 'vuex'
import BalanceModule from './modules/balanceModule'
import ProductModule from './modules/productModule'
import CartModule from './modules/cartModule'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    BalanceModule,
    ProductModule,
    CartModule
  }
})