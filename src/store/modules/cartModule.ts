import { VuexModule, Module, Action, Mutation } from 'vuex-module-decorators'

export interface ICart {
  productId: number
}

@Module({ namespaced: true })
export default class CartModule extends VuexModule {
  
  public carts: ICart = {productId: 0}

  public get getCarts(): ICart {
    return this.carts
  }

  @Mutation
  public addToCart(payload: number): void {
    this.carts = ({ productId: payload })
  }
  
  @Action
  public addProductToCart(payload: number): void {
    const { commit } = this.context

    commit('addToCart', payload)
  }
}