import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'

@Module({ namespaced: true })
export default class BalancaModule extends VuexModule {
  public balance: number = 0

  public allowedMoney: any = [2000, 5000, 10000, 20000, 50000]

  public get getBalance(): number {
    return this.balance
  }

  public get getAllowedMoney(): any {
    return this.allowedMoney
  }

  @Mutation
  public incrementBalance(amount: number): void {
    this.balance += amount
  }

  @Mutation
  public decrementBalance(amount: number): void {
    this.balance -= amount
  }

  @Action
  public setIncrementBalance(payload: any): void {
    const { commit } = this.context

    commit('incrementBalance', parseInt(payload))
  }

  @Action
  public setDecrementBalance(payload: any): void {
    const { commit } = this.context

    commit('decrementBalance', parseInt(payload))
  }
}
