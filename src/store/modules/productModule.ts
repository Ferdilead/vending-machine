import { Module, VuexModule, Mutation, Action } from 'vuex-module-decorators'

export interface IProduct {
  productId: number,
  name: string,
  price: number,
  qty: number
}

@Module({ namespaced: true })
export default class ProductModule extends VuexModule {
  public products: IProduct[] = [
    { productId: 1, name: 'Biscout', price: 6000, qty: 10 },
    { productId: 2, name: 'Chips', price: 8000, qty: 5 },
    { productId: 3, name: 'Oreo', price: 10000, qty: 2 },
    { productId: 4, name: 'Tango', price: 12000, qty: 1 },
    { productId: 5, name: 'Coklat', price: 15000, qty: 3 }
  ]

  public get getProduct(): IProduct[] {
    return this.products
  }

  @Mutation
  public changeProductQty(productId: number): void {
    this.products[productId].qty -= 1
  }

  @Action
  public setProductQty(payload: any): void {
    const { commit } = this.context

    commit('changeProductQty', parseInt(payload) - 1)
  }
}