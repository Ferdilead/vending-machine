import { Vue, Component }from 'vue-property-decorator'
import WithRender from './template.html?style=./style.scss'
import AccountBalance from '@/components/AccountBalance'
import Product from '@/components/Product'
import Checkout from '@/components/Checkout'

@WithRender
@Component({
  components: {
    AccountBalance,
    Product,
    Checkout
  }
})
export default class Login extends Vue {
  public title: string = 'Vending Machine'
}
