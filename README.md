## Instalation
```shell
npm install
```

## Start App
```shell
npm run dev
```

## Open in browser
```shell
http://localhost:3000
```